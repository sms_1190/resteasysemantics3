import java.io.IOException;

import org.json.JSONObject;

import com.semantics3.api.Products;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;



public class Example {

	/**
	 * @param args
	 */
	public static void main(String args[]) {

		try {
			Products products = new Products(
					"SEM34E01734668B4ED25870A8DD8FB0F0DB3",
					"ZTk1ODA3YmJlODA0ZDFlNWQ3YTYzNmU4NWE0NTJhZTc"
				);
			products.field( "cat_id", 17366 );
			
			JSONObject results;	
			results = products.get();
			System.out.println(results.toString(4));
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException
				| OAuthCommunicationException | IOException e) {
			e.printStackTrace();
		}

	}

}
