package com.semantics3.main;

import java.io.IOException;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.json.JSONObject;

import com.javacodegeeks.resteasy.model.Query;
import com.semantics3.api.Products;

public class Semantics3 {

	public JSONObject searchQuery(Query query) {
		JSONObject results = null;

		try {
			Products products = new Products(
					"SEM3C7E335D48C811F11B810F87D9B374D91",
					"YzNlN2E4ZTkzMmI3ZDZmMmFiM2IxZTQ2YTQ4MzY2NzI");
			products
			.field("cat_id", query.getCategoryID())
			.field("name", "include", query.getSearchName());

			if (query.getMaxPrice() != null && query.getMaxPrice() > 0) {
				products.field("price", "lt", query.getMaxPrice());
			}
			if (query.getMinPrice() != null && query.getMinPrice() > 0) {
				products.field("price", "gte", query.getMinPrice());
			}
			if (query.getOffSet() != null && query.getOffSet() > 0) {
				products.field("offset", query.getOffSet());
			}
			if (query.getLimit() != null && query.getLimit() > 0) {
				products.field("limit", query.getLimit());
			}
			if (query.getColor() != null && query.getColor() != "") {
				products.field("color", query.getColor());
			}
			if (query.getBrand() != null && query.getBrand() != "") {
				products.field("brand", query.getBrand());
			}
			results = products.get();
			System.out.println(results.toString(4));
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException
				| OAuthCommunicationException | IOException e) {
			e.printStackTrace();
		}
		return results;

	}
}
