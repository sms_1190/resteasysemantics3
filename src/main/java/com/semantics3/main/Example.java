package com.semantics3.main;
import java.io.IOException;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.json.JSONObject;

import com.semantics3.api.Products;

public class Example {

	/**
	 * @param args
	 */
	public static void main(String args[]) {

		try {
			Products products = new Products(
					"SEM3C7E335D48C811F11B810F87D9B374D91",
					"YzNlN2E4ZTkzMmI3ZDZmMmFiM2IxZTQ2YTQ4MzY2NzI"
				);
			products
			.field( "cat_id", 17366 )
			.field("price", "gte", 100)
			.field("name","ski jacket")
			.field("price", "lt", 500)
			.field("offset",0);
			//.field("color","");
			//.field("limit",10);
			JSONObject results;	
			results = products.get();
			System.out.println(results.toString(4));
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException
				| OAuthCommunicationException | IOException e) {
			e.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
		}

	}

}
