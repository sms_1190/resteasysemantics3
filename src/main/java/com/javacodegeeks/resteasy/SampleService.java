package com.javacodegeeks.resteasy;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONObject;

import com.evocatus.amazonclient.ItemLookupSample;
import com.javacodegeeks.resteasy.model.Query;
import com.semantics3.errors.Semantics3Exception;
import com.semantics3.main.Semantics3;

/**
 * @File Name : SampleService.java
 * @Description : Class for creating different REST request receivers
 * @author Vaibhav
 * 
 */
@Path("/sampleservice")
public class SampleService {

	@POST
	@Path("/amazon")
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/xml")
	public String itemLookup(MultivaluedMap<String, String> formParams){
		ItemLookupSample itemLookup = new ItemLookupSample();
		System.out.println(formParams.get("searchIndex").get(0));
		String output = itemLookup.ItemSearch(formParams.get("searchIndex").get(0), formParams.get("title").get(0));
		return output;
	}
	
	
	
	
	@POST
	@Path("/s")
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/text")
	public String post(MultivaluedMap<String, String> formParams) {
		Query query = new Query();
		if(formParams.containsKey("search_name")){
	    	System.out.println(formParams.get("search_name"));
	    	query.setSearchName(formParams.get("search_name").get(0));
	    }
		if(formParams.containsKey("minprice")){
	    	System.out.println(formParams.get("minprice"));
	    	query.setMinPrice(Integer.parseInt(formParams.get("minprice").get(0)));
	    }
		if(formParams.containsKey("maxprice")){
	    	System.out.println(formParams.get("maxprice"));
	    	query.setMaxPrice(Integer.parseInt(formParams.get("maxprice").get(0)));
	    }
		if(formParams.containsKey("offset")){
	    	System.out.println(formParams.get("offset"));
	    	query.setOffSet(Integer.parseInt(formParams.get("offset").get(0)));
	    }
		if(formParams.containsKey("limit")){
	    	System.out.println(formParams.get("limit"));
	    	query.setLimit(Integer.parseInt(formParams.get("limit").get(0)));
	    }
		if(formParams.containsKey("color")){
	    	System.out.println(formParams.get("color"));
	    	query.setColor(formParams.get("color").get(0));
	    }
		if(formParams.containsKey("brand")){
	    	System.out.println(formParams.get("brand"));
	    	query.setBrand(formParams.get("brand").get(0));
	    }
		if(formParams.containsKey("cat_id")){
	    	System.out.println(formParams.get("cat_id"));
	    	query.setCategoryID(Integer.parseInt(formParams.get("cat_id").get(0)));
	    }
		
		Semantics3 queryMain = new Semantics3();
		JSONObject results = queryMain.searchQuery(query);
		return results.toString();
	}
}