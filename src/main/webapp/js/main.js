$(function() {
	var currentValue = $('#limitv');

	$('#limit').change(function() {
		currentValue.html(this.value);
	});

	// Trigger the event on load, so
	// the value field is populated:

	$('#limit').change();

	$("#data_button")
			.click(
					function() {
						var d = "";
						d = d + "search_name=" + $("#search").val();
						if ($("#cat_id_c").is(":checked")) {
							d = d + "&cat_id=" + $("#cat_id").val();
						}
						if ($("#price_c").is(":checked")) {
							d = d + "&minprice=" + $("#minprice").val();
							d = d + "&maxprice=" + $("#maxprice").val();
						}
						if ($("#brand_c").is(":checked")) {
							d = d + "&brand=" + $("#brand").val();
						}
						if ($("#color_c").is(":checked")) {
							d = d + "&color=" + $("#color").val();
						}
						if ($("#offset_c").is(":checked")) {
							d = d + "&offset=" + $("#offset").val();
						}
						if ($("#limit_c").is(":checked")) {
							d = d + "&limit=" + $("#limit").val();
						}

						$
								.ajax({
									type : "POST",
									url : "http://localhost:8080/resteasysemantics3/restful-services/sampleservice/s",
									data : d,
									contentType : 'application/x-www-form-urlencoded',
									success : function(result) {
										console.log(vkbeautify.json(result));
										$("#pr").text(vkbeautify.json(result));
										JSONtoItem(result);
									},
									async : false
								});
					});

	$("#amazon_search")
			.click(
					function() {
						var d = "";
						d = d + "title=" + $("#searchTerm").val();
						d = d + "&searchIndex=" + $("#searchIndex").val();

						$
								.ajax({
									type : "POST",
									url : "http://localhost:8080/resteasysemantics3/restful-services/sampleservice/amazon",
									data : d,
									contentType : 'application/x-www-form-urlencoded',
									success : function(result) {
										abc = result;
										console.log(result);
										c = $.xml2json(result);
										$("#pr").text(vkbeautify.json(c.Items));
										createTable(result);
									},
									async : false
								});

					});
	
	$("#showTextArea").click(function(){
		$("#pr").toggle();
	});
	

});

function JSONtoItem(result) {
	var jsonOutput = JSON.parse(result);
	console.log(jsonOutput);
	$("#results").html("");
	// var rows = $("<div class='row-fluid'></div>");
	for ( var i = 0; i < jsonOutput.results.length; i++) {

		var name = jsonOutput.results[i].name;
		var price = jsonOutput.results[i].price;
		var brand = jsonOutput.results[i].brand;
		var features = jsonOutput.results[i].features;
		var upc = jsonOutput.results[i].upc;
		var color = jsonOutput.results[i].color;
		var size = jsonOutput.results[i].size;
		var d = $("<div class=\"span4\" style=\"margin: 4px;padding: 4px;border:1px solid black; border-radius:5px;\"></div>");
		var t = $("<table class=\"table table-striped\"></table>");
		d.append(t);
		t.append("<tr><Td><b>Name&nbsp;: </b></td><td>" + name + "</td></tr>");
		t
				.append("<tr><Td><b>Price&nbsp;: </b></td><td>" + price
						+ "</td></tr>");
		t
				.append("<tr><Td><b>Brand&nbsp;: </b></td><td>" + brand
						+ "</td></tr>");

		if (typeof features != 'undefined') {
			var e = $("<tr></tr>");
			e.append("<Td><b>Features&nbsp;: </b></tD>");
			var s = $("<td></td>");
			$.each(features, function(key, value) {
				s.append("<p>" + key + " &nbsp;: " + value + "</p>");
			});
			e.append(s);
			t.append(e);
		}

		t.append("<tr><Td><b>upc&nbsp;: </b></td><td>" + upc + "</td></tr>");
		t
				.append("<tr><Td><b>Color&nbsp;: </b></td><td>" + color
						+ "</td></tr>");
		t.append("<tr><Td><b>Size&nbsp;: </b></td><td>" + size + "</td></tr>");

		$("#results").append(d);
	}

}

function createTable(result) {
	$("#results").html("");
	var items = result.getElementsByTagName("Item");

	for ( var i = 0; i < items.length; i++) {
		console.log(i);
		var name = items[i].getElementsByTagName("Title")[0].textContent;
		if (items[i].getElementsByTagName("FormattedPrice").length > 0) {
			var price = items[i].getElementsByTagName("FormattedPrice")[0].textContent;
		}
		var brand = items[i].getElementsByTagName("Brand")[0].textContent;

		if (items[i].getElementsByTagName("Feature").length > 0) {
			var features = items[i].getElementsByTagName("Feature");
		}
		var asin = items[i].getElementsByTagName("ASIN")[0].textContent;
		if (items[i].getElementsByTagName("Color").length > 0) {
			var color = items[i].getElementsByTagName("Color")[0].textContent;
		}
		var size = "";
		if (items[i].getElementsByTagName("Size").length != 0) {
			size = items[i].getElementsByTagName("Size")[0].textContent;
		}
		if (items[i].getElementsByTagName("SmallImage").length > 0) {
			var image = items[i].getElementsByTagName("SmallImage")[0]
					.getElementsByTagName("URL")[0].textContent;
		}

		var d = $("<div class=\"span4\" style=\"margin: 4px;padding: 4px;border:1px solid black; border-radius:5px;\"></div>");
		var t = $("<table class=\"table table-striped\"></table>");
		d.append(t);
		t.append("<tr><td><b>ASIN&nbsp;: </b></td><td>" + asin + "</td></tr>");
		t.append("<tr><td><b>Name&nbsp;: </b></td><td>" + name + "</td></tr>");
		t
				.append("<tr><td><b>Price&nbsp;: </b></td><td>" + price
						+ "</td></tr>");
		t
				.append("<tr><td><b>Brand&nbsp;: </b></td><td>" + brand
						+ "</td></tr>");

		if (features.length > 0) {

			b = $("<tr></tr>");
			b.append("<td><b>Feature&nbsp;: </b></td>");
			a = $("<td></td>");

			for ( var j = 0; j < features.length; j++) {
				a.append("<p>" + features[j].textContent + "</p>");
			}
			b.append(a);
			t.append(b);
		}

		t
				.append("<tr><td><b>Color&nbsp;: </b></td><td>" + color
						+ "</td></tr>");
		t.append("<tr><td><b>Size&nbsp;: </b></td><td>" + size + "</td></tr>");
		t.append("<tr><td><b>Image&nbsp;: </b></td><td><img src='" + image
				+ "' /></td></tr>");

		$("#results").append(d);

	}

}
